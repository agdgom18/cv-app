import React from 'react';
import Panel from '../components/Panel';
import Informaiton from '../components/Informaiton';
import '../scss/blocks/_home.scss';

const Home = () => {
  return (
    <div className="container">
      <Panel />
      <main>
        <Informaiton />
      </main>
    </div>
  );
};

export default Home;
