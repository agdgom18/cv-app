import React from 'react';
import '../scss/blocks/_notFound.scss';
import { Link } from 'react-router-dom';
const Notfound = () => {
  return (
    <div className="not-found">
      <h1 className="title--not">404</h1>
      <p className="desc--not">Oops! Something is wrong.</p>

      <div className="link-go__home">
        <Link className="button--not" to="/">
          Go back in home page, is better.
        </Link>
      </div>
    </div>
  );
};

export default Notfound;
