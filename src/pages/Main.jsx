import React from 'react';
import '../scss/blocks/_mainPage.scss';
import userImg from '../assets/img/user-avatar.png';
import { Link } from 'react-router-dom';
import Button from '../components/Button';
import PhotoBox from '../components/PhotoBox';
const Main = () => {
  const imgSize = { width: '163px', height: '163px' };

  return (
    <div className="main">
      <div className="main__wrapper">
        <PhotoBox
          isSidePanel={false}
          size={imgSize}
          avatar={userImg}
          name="John Doe"
          title="Programmer. Creative. Innovator"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
        />

        <Link to="/home">
          <Button text="Know more" />
        </Link>
      </div>
    </div>
  );
};

export default Main;
