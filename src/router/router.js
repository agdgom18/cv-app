import Informaiton from '../components/Informaiton';
import Home from '../pages/Home';
import MainPage from '../pages/Main';
import Notfound from '../pages/Notfound';

const router = [
  {
    element: <MainPage />,
    path: '/',
  },
  {
    element: <Home />,
    path: '/home',
  },
  {
    element: <Informaiton />,
    path: '/Info',
  },
  {
    element: <Notfound />,
    path: '*',
  },
];

export default router;
