import { Formik, Form, ErrorMessage, Field } from 'formik';
import * as Yup from 'yup';

import React from 'react';
import '../scss/blocks/form.scss';
import { useDispatch } from 'react-redux';
import { updateSkillsList } from '../store/skillsSlice';

const EditForm = () => {
  const dispatch = useDispatch();
  const initialValues = {
    skill: '',
    range: '',
  };
  const validationSchema = Yup.object({
    skill: Yup.string().required('Skill is required'),
    range: Yup.number().min(10, ' Range must be more than 10!').max(100, ' Range must be less 100!').required(' Skill range must be number type '),
  });

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={(values, { resetForm }) => {
          dispatch(updateSkillsList({ name: values.skill, level: values.range }));
          resetForm();
        }}
        validationSchema={validationSchema}>
        {({ errors, touched, isValid, dirty }) => (
          <Form>
            <div className="field">
              <label className="form-label">Skill name</label>
              <Field
                className="input-value"
                style={{ border: errors.skill && touched.skill ? '1px solid red' : '' }}
                name="skill"
                placeholder="Skill"
                autoComplete="off"
              />

              <div className="error">
                <ErrorMessage name="skill" component="span" />
              </div>
            </div>
            <div className="field">
              <label className="form-label">Skill range</label>
              <Field
                className="input-value"
                style={{ border: errors.range && touched.range ? '1px solid red' : '' }}
                name="range"
                placeholder="Range"
                autoComplete="off"
              />
              <div className="error">
                <ErrorMessage name="range" component="span" />
              </div>
            </div>
            <button
              type="submit"
              style={{ backgroundColor: !isValid || !dirty ? '#222935' : '#13b96c' }}
              disabled={!isValid || !dirty}
              className="form-btn">
              Submit
            </button>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default EditForm;
