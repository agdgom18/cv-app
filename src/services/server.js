import { Server } from 'miragejs';
import { edList } from './data';
import { skillsDataList } from './data';

export function makeServer() {
  let server = new Server({
    routes() {
      // GET
      this.get(
        '/api/education',
        () => {
          return edList;
        },
        { timing: 3000 },
      );
      this.get(
        '/api/skills',
        () => {
          return skillsDataList;
        },
        { timing: 1000 },
      );

      this.post('/api/skills', (schema, request) => {
        const attrs = JSON.parse(request.requestBody);
        attrs.id = Math.floor(Math.random() * 10000);
        skillsDataList.push(attrs);
        return skillsDataList;
      });
      // DELETE User
      //   this.delete('/api/users/:id', (schema, request) => {
      //     const id = request.params.id;
      //     return schema.users.find(id).destroy();
      //   });
    },
  });
  return server;
}
