import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import Feedback from './components/Feedback';
import App from './App';
import { fetchData } from './store/dataSlice';
import dataSlice from './store/dataSlice';
import { BrowserRouter as Router } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Box from './components/Box';
import scrollToTop from './utils/scroll';
import EditForm from './utils/EditForm';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import Skills from './components/Skills';
import { store } from './store/store';
import Experience from './components/Experience';
import Panel from './components/Panel';
import { expDataList } from './services/data';

describe('Form test', () => {
  const validationSchema = Yup.object({
    skill: Yup.string().required('Skill is required'),
    range: Yup.number()
      .typeError('Skill range must be a number')
      .required('Skill range is required')
      .min(10, 'Skill range must be at least 10')
      .max(100, 'Skill range must not exceed 100'),
  });

  it('EditForm component renders correctly', () => {
    render(
      <Provider store={store}>
        <EditForm />
      </Provider>,
    );

    act(() => {
      const skillInput = screen.getByPlaceholderText('Skill');
      const rangeInput = screen.getByPlaceholderText('Range');

      expect(skillInput).toBeInTheDocument();
      expect(rangeInput).toBeInTheDocument();

      const submitButton = screen.getByText('Submit');
      expect(submitButton).toBeInTheDocument();
    });
  });

  it('EditForm component handles form submission', () => {
    const dispatch = jest.fn();
    render(
      <Formik
        initialValues={{ skill: '', range: '' }}
        onSubmit={(values, { resetForm }) => {
          dispatch(values);
        }}
        validationSchema={validationSchema}>
        <Form>
          <Field name="skill" placeholder="skill" />
          <Field name="range" placeholder="range" />
          <button type="submit">Submit</button>
        </Form>
      </Formik>,
    );

    act(() => {
      const skillInput = screen.getByPlaceholderText('skill');
      const rangeInput = screen.getByPlaceholderText('range');

      fireEvent.change(skillInput, { target: { value: 'Test Skill' } });
      fireEvent.change(rangeInput, { target: { value: '50' } });

      const submitButton = screen.getByText('Submit');
      fireEvent.click(submitButton);
    });
  });
});

describe('Skills Component', () => {
  it('renders skills list', () => {
    render(
      <Provider store={store}>
        <Skills />
      </Provider>,
    );

    const skillsList = screen.getByText('Beginner');
    expect(skillsList).toBeInTheDocument();
  });

  it('displays skill names', () => {
    render(
      <Provider store={store}>
        <Skills />
      </Provider>,
    );

    const skillNames = screen.getByText(/Proficient/);
    expect(skillNames).toBeInTheDocument();
  });

  test('renders skill levels', () => {
    render(
      <Provider store={store}>
        <Skills />
      </Provider>,
    );

    const skillLevels = screen.getByText('Master');
    expect(skillLevels).toBeInTheDocument();
  });
});

describe('test panel component', () => {
  it('renders Panel component', () => {
    render(
      <BrowserRouter>
        <Panel />
      </BrowserRouter>,
    );

    const panelElement = screen.getByTestId('asideElement');
    const backButton = screen.getByText('Go back');
    expect(panelElement).toHaveClass('expand');
    expect(backButton).toBeInTheDocument();
  });

  it('toggles panel state', () => {
    render(
      <BrowserRouter>
        <Panel />
      </BrowserRouter>,
    );

    const panelElement = screen.getByTestId('asideElement');
    expect(panelElement).toHaveClass('expand');
    const toggleButton = screen.getByTestId('toggleElement');
    fireEvent.click(toggleButton);
    expect(panelElement).toHaveClass('collapse');
  });
});
describe('test experience component', () => {
  it('renders Experience component', () => {
    render(<Experience />);
    const experienceSection = screen.getByText('Twitter');

    expect(experienceSection).toBeInTheDocument();
  });

  it('renders experience items', () => {
    render(<Experience />);
    const experienceItems = screen.getAllByText(/developer/i);
    expect(experienceItems).toHaveLength(expDataList.length);
  });
});

describe('test Store', () => {
  it('should handle fetchUsers.pending', () => {
    const action = fetchData.pending();
    const state = { loading: false, users: [], error: '' };
    const newState = dataSlice(state, action);
    expect(newState.loading).toBe(true);
  });
});

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Programmer/i);
  expect(linkElement).toBeInTheDocument();
});

test('Feedback component renders correctly', () => {
  render(
    <Router>
      <Feedback />
    </Router>,
  );

  const citeElement = screen.getAllByText('https://www.citeexample.com');

  expect(citeElement).toHaveLength(2);
});

describe('scrollToTop', () => {
  it('scrolls to the top of the page', () => {
    const originalWindowScrollTo = window.scrollTo;
    window.scrollTo = jest.fn();
    scrollToTop();

    expect(window.scrollTo).toHaveBeenCalledWith({
      top: 0,
      behavior: 'smooth',
    });
    window.scrollTo = originalWindowScrollTo;
  });
});

test('Box component renders correctly', () => {
  const mockStore = configureStore([]);
  const store = mockStore({});
  const title = 'Test Title';
  const content = 'Test Content';
  render(
    <Provider store={store}>
      <Box title={title} content={content} isEdit={true} />
    </Provider>,
  );

  const titleElement = screen.getByText(title);
  const contentElement = screen.getByText(content);

  expect(titleElement).toBeInTheDocument();
  expect(contentElement).toBeInTheDocument();

  const editButton = screen.getByText('Open edit');
  expect(editButton).toBeInTheDocument();

  fireEvent.click(editButton);
});
