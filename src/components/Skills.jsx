import React from 'react';
import '../scss/blocks/_skills.scss';
import { useSelector, useDispatch } from 'react-redux';
import { fetchSkills } from '../store/skillsSlice';
import EditForm from '../utils/EditForm';

const Skills = () => {
  const skillList = useSelector((state) => state.skills.data);
  const editSkills = useSelector((state) => state.editStore.editSkills);
  let renderContent = null;
  const dispatch = useDispatch();
  if (!localStorage.getItem('reduxState')) {
    dispatch(fetchSkills());
    renderContent = skillList;
  } else {
    renderContent = JSON.parse(localStorage.getItem('reduxState')).skills.data;
  }
  // React.useEffect(() => {
  //   dispatch(fetchSkills());
  // }, [dispatch]);

  return (
    <section id="skills">
      {editSkills && (
        <div className="add-skills-block">
          <EditForm />
        </div>
      )}
      <ul className="skills-list">
        {renderContent &&
          renderContent.map((skill, idx) => {
            return (
              <li style={{ width: `${skill.level}%`, minWidth: '71px' }} key={idx}>
                <p title={skill.name}>{skill.name}</p>
              </li>
            );
          })}
      </ul>
      <ul className="skills-level">
        <li>Beginner</li>
        <li>Proficient</li>
        <li>Expert</li>
        <li>Master</li>
      </ul>
    </section>
  );
};

export default Skills;
