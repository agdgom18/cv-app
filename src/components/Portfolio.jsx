import React from 'react';
import Isotope from 'isotope-layout';
import '../scss/blocks/_portfolio.scss';
import img1 from '../assets/img/card_1.jpg';
import img2 from '../assets/img/card_2.jpg';
import { Link } from 'react-router-dom';
import { initialFilters } from '../services/data';
const Portfolio = () => {
  // init one ref to store the future isotope object
  const isotope = React.useRef();
  // store the filter keyword in a state
  const [filters, setFilters] = React.useState(initialFilters);
  // initialize an Isotope object with configs
  React.useEffect(() => {
    isotope.current = new Isotope('.filter-container', {
      itemSelector: '.filter-item',
      layoutMode: 'fitRows',
    });
    // cleanup
    return () => isotope.current.destroy();
  }, []);

  // handling filter key change
  React.useEffect(() => {
    const { filterName } = filters.find((el) => el.isChecked);

    isotope.current.arrange({ filter: filterName === '*' ? '*' : `.${filterName}` });
  }, [filters]);

  const handleFilterKeyChange = (key) => () => {
    const newFilterState = filters.map((el) => {
      if (el.filterName === key) {
        return { ...el, isChecked: true };
      } else {
        return { ...el, isChecked: false };
      }
    });
    setFilters(newFilterState);
  };

  return (
    <section className="portfolio">
      <div className="portfolio__btn-list">
        {filters.map((filter, idx) => {
          return (
            <button
              className={filter.isChecked ? 'portfolio__btn active' : 'portfolio__btn'}
              key={idx}
              onClick={handleFilterKeyChange(filter.filterName)}>
              {filter.name}
            </button>
          );
        })}
      </div>

      <ul className="filter-container">
        <div className="filter-item code">
          <div className="card" style={{ backgroundImage: `url(${img1})` }}>
            <div className="card__content">
              <h4 className="card__title">Some text</h4>
              <p className="card__desc">
                Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                Nullam dictum felis eu pede mollis
              </p>
              <Link className="card__link">View resource</Link>
            </div>
          </div>
        </div>
        <div className="filter-item ui">
          <div className="card" style={{ backgroundImage: `url(${img2})` }}>
            <div className="card__content">
              <h4 className="card__title">Some text</h4>
              <p className="card__desc">
                Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                Nullam dictum felis eu pede mollis
              </p>
              <Link className="card__link">View resource</Link>
            </div>
          </div>
        </div>
        <div className="filter-item code">
          <div className="card" style={{ backgroundImage: `url(${img1})` }}>
            <div className="card__content">
              <h4 className="card__title">Some text</h4>
              <p className="card__desc">
                Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                Nullam dictum felis eu pede mollis
              </p>
              <Link className="card__link">View resource</Link>
            </div>
          </div>
        </div>
        <div className="filter-item ui">
          <div className="card" style={{ backgroundImage: `url(${img2})` }}>
            <div className="card__content">
              <h4 className="card__title">Some text</h4>
              <p className="card__desc">
                Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                Nullam dictum felis eu pede mollis
              </p>
              <Link className="card__link">View resource</Link>
            </div>
          </div>
        </div>
      </ul>
    </section>
  );
};

export default Portfolio;
