import React from 'react';
import Navigation from './Navigation';
import PhotoBox from './PhotoBox';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import avatar from '../assets/img/user-avatar.png';
import '../scss/blocks/_panel.scss';
import Button from './Button';
import { NavLink } from 'react-router-dom';
const Panel = () => {
  const [isOpen, setIsOpen] = React.useState(true);
  const toggle = () => setIsOpen(!isOpen);

  const imgSize = { maxWidth: '100px', maxHeight: '100px' };
  return (
    <aside data-testid="asideElement" className={`panel ${isOpen ? 'expand' : 'collapse'}`}>
      <div className="panel-wrapper">
        <div data-testid="toggleElement" onClick={toggle} className="bars">
          <FontAwesomeIcon icon={faBars} style={{ color: '#fff' }} />
        </div>
        <div className="panel__photobox-wrapper">
          <PhotoBox isSidePanel={true} size={imgSize} name={'Joe Doe'} avatar={avatar} />
        </div>
        <Navigation />
      </div>
      <NavLink to="/" className="back-btn">
        <Button isClose={!isOpen} text="Go back" icon={<FontAwesomeIcon icon={faChevronLeft} />} />
      </NavLink>
    </aside>
  );
};

export default Panel;
