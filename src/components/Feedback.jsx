import React from 'react';
import '../scss/blocks/_feedback.scss';
import { feedbackList } from '../services/data';
import reporterAvatar from '../assets/img/feedbackAvatar.jpg';
import { Link } from 'react-router-dom';
const Feedback = () => {
  return (
    <section className="feedback">
      <ul className="feedback__list">
        {feedbackList.map((item, idx) => {
          return (
            <li className="feedback__list--item" key={idx}>
              <p className="feedback__desc">{item.feedback}</p>
              <div className="feedback__reporter--info">
                <img className="feedback__avatar" src={reporterAvatar} alt="avatar" />
                <h4 className="feedback__title">
                  {item.reporter.name},<Link className="feedback__link"> {item.reporter.citeUrl}</Link>
                </h4>
              </div>
            </li>
          );
        })}
      </ul>
    </section>
  );
};

export default Feedback;
