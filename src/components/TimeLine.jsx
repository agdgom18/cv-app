import React from 'react';
import '../scss/blocks/_timeLine.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSelector, useDispatch } from 'react-redux';
import { fetchData } from '../store/dataSlice';
import { faRotate } from '@fortawesome/free-solid-svg-icons';
const TimeLine = () => {
  const loading = useSelector((state) => state.education.loading);
  const edItem = useSelector((state) => state.education);
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(fetchData());
  }, [dispatch]);

  return (
    <section className="time__line">
      {loading ? (
        <div className="loading__icon">
          <FontAwesomeIcon color="#26c17e" icon={faRotate} />
        </div>
      ) : (
        <ul className="ed__list">
          {edItem.data.map((item, index) => {
            return (
              <li key={index} className="ed__item">
                <div className="ed__time">
                  <h3 className="data-title">{item.date}</h3>
                  <div className="ed__vertical-line"></div>
                </div>
                <div className="ed__info">
                  <div className="ed__box">
                    <h4 className="ed__title">{item.title}</h4>
                    <p className="ed__desc">{item.text}</p>
                  </div>
                </div>
              </li>
            );
          })}
        </ul>
      )}
    </section>
  );
};

export default TimeLine;
