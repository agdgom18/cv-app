import React from 'react';
import '../scss/blocks/_experience.scss';
import { expDataList } from '../services/data';
const Experience = () => {
  return (
    <section className="experience">
      {expDataList.map((item, index) => {
        return (
          <div key={index} className="experience__wrapper">
            <div className="experience__info">
              <h3 className="info__title">{item.info.company}</h3>
              <p className="info__year">{item.date}</p>
            </div>
            <div className="experience__desc">
              <h3 className="info__title">{item.info.job}</h3>
              <p className="desc__desc">{item.info.description}</p>
            </div>
          </div>
        );
      })}
    </section>
  );
};

export default Experience;
