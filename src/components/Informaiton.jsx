import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import Box from './Box';
import TimeLine from './TimeLine';
import Experience from './Experience';
import Skills from './Skills';
import Contacts from './Contacts';
import Feedback from './Feedback';
import Button from './Button';
import { Link } from 'react-router-dom';
import Portfolio from './Portfolio';
import scrollToTop from '../utils/scroll';
const Informaiton = () => {
  return (
    <div>
      <Box
        id="about"
        title="About us"
        content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque"
      />

      <Box id={'education'} title="Education">
        <TimeLine />
      </Box>
      <Box id={'experience'} title="Experience">
        <Experience />
      </Box>

      <Box id={'skills'} title="Skills" isEdit={true}>
        <Skills />
      </Box>
      <Box id={'portfolio'} title="Portfolio">
        <Portfolio />
      </Box>
      <Box id={'contacts'} title="Contacts">
        <Contacts />
      </Box>
      <Box id={'feedbacks'} title="Feedbacks">
        <Feedback />
      </Box>
      <Link onClick={scrollToTop} className="btn--top">
        <Button icon={<FontAwesomeIcon icon={faChevronUp} />} />
      </Link>
    </div>
  );
};

export default Informaiton;
