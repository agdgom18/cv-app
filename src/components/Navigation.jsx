import React from 'react';
import '../scss/blocks/_navigation.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faGraduationCap, faPencil, faGem, faSuitcase, faLocationArrow, faComment } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-scroll';

const Navigation = () => {
  return (
    <nav>
      <li>
        <Link className="link" to="about" spy={true} smooth={true} offset={-100} duration={50}>
          <div className="icon">
            <FontAwesomeIcon icon={faUser} />
          </div>
          <div className="link-text">About me</div>
        </Link>
      </li>
      <li>
        <Link className="link" to="education" spy={true} smooth={true} offset={-100} duration={50}>
          <div className="icon">
            <FontAwesomeIcon icon={faGraduationCap} />
          </div>
          <div className="link-text">Education</div>
        </Link>
      </li>
      <li>
        <Link className="link" to="experience" spy={true} smooth={true} offset={-100} duration={50}>
          <div className="icon">
            <FontAwesomeIcon icon={faPencil} />
          </div>
          <div className="link-text">Experience</div>
        </Link>
      </li>

      <li>
        <Link className="link" to="skills" spy={true} smooth={true} offset={-100} duration={50}>
          <div className="icon">
            <FontAwesomeIcon icon={faGem} />
          </div>
          <div className="link-text">Skills</div>
        </Link>
      </li>
      <li>
        <Link className="link" to="portfolio" spy={true} smooth={true} offset={-100} duration={50}>
          <div className="icon">
            <FontAwesomeIcon icon={faSuitcase} />
          </div>
          <div className="link-text">Portfolio</div>
        </Link>
      </li>

      <li>
        <Link className="link" to="contacts" spy={true} smooth={true} offset={-100} duration={50}>
          <div className="icon">
            <FontAwesomeIcon icon={faLocationArrow} />
          </div>
          <div className="link-text">Contacts</div>
        </Link>
      </li>
      <li>
        <Link className="link" to="feedbacks" spy={true} smooth={true} offset={-100} duration={50}>
          <div className="icon">
            <FontAwesomeIcon icon={faComment} />
          </div>
          <div className="link-text">Feedbacks</div>
        </Link>
      </li>
    </nav>
  );
};

export default Navigation;
