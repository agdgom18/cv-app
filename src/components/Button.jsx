import React from 'react';
import '../scss/blocks/_button.scss';

const Button = ({ icon, text, isClose }) => {
  return (
    <>
      <button className="btn">
        {icon}
        <span> {isClose ? '' : text}</span>
      </button>
    </>
  );
};

export default Button;
