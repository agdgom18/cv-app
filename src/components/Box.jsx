import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../scss/blocks/_box.scss';
import Button from './Button';
import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from 'react-redux';
import { changeEditStatusById } from '../store/editSlice';

const Box = ({ title, content, children, id, isEdit = false }) => {
  const dispatch = useDispatch();

  const editSection = () => {
    if (id) {
      dispatch(changeEditStatusById(id));
    }
  };
  return (
    <section className={`${title} box-wrapper`} id={id}>
      <div className="box__title">
        <h2>{title}</h2>
        {isEdit && (
          <div onClick={editSection}>
            <Button icon={<FontAwesomeIcon icon={faPenToSquare} />} text="Open edit" />
          </div>
        )}
      </div>
      <p className="box__content">{content}</p>
      {children}
    </section>
  );
};

export default Box;
