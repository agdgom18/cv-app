import React from 'react';
import '../scss/blocks/_contacts.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faTwitter, faFacebook, faSkype } from '@fortawesome/free-brands-svg-icons';
import { Link } from 'react-router-dom';
const Contacts = () => {
  return (
    <section className="contacts">
      <ul className="constact__list">
        <li className="list__item">
          <FontAwesomeIcon className="contact__icon" style={{ color: '#26C17E' }} icon={faPhone} />
          <h4>
            <Link to="tel:+1234567890">500 342 242</Link>
          </h4>
        </li>
        <li className="list__item">
          <FontAwesomeIcon className="contact__icon" style={{ color: '#26C17E' }} icon={faEnvelope} />
          <h4>
            <Link to="mailto:email@example.com">office@kamsolutions.pl</Link>
          </h4>
        </li>
        <li className="list__item">
          <FontAwesomeIcon className="contact__icon" style={{ color: '#26C17E' }} icon={faTwitter} />
          <div>
            <Link to="https://twitter.com/wordpress">
              <h4>Twitter</h4>
              <p>https://twitter.com/wordpress</p>
            </Link>
          </div>
        </li>
        <li className="list__item">
          <FontAwesomeIcon className="contact__icon" style={{ color: '#26C17E' }} icon={faFacebook} />
          <div>
            <Link to="https://www.facebook.com/facebook">
              <h4>Facebook</h4>
              <p>https://www.facebook.com/facebook</p>
            </Link>
          </div>
        </li>
        <li className="list__item">
          <FontAwesomeIcon className="contact__icon" style={{ color: '#26C17E' }} icon={faSkype} />
          <div>
            <h4>Skype </h4>
            <p>kamsolutions.pl</p>
          </div>
        </li>
      </ul>
    </section>
  );
};

export default Contacts;
