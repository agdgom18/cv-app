import '../scss/blocks/_photobox.scss';

const PhotoBox = ({ avatar, size, name, title, description, isSidePanel }) => {
  return (
    <>
      <img className="photobox__img" src={avatar} alt="user-img" style={size} />
      <h1 className={`title ${isSidePanel ? 'title--small' : ''}`}>{name}</h1>
      <p className="subtitle">{title}</p>
      <p className="desc">{description}</p>
    </>
  );
};
export default PhotoBox;
