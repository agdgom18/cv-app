import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const initialState = {
  loading: true,
  data: [],
  error: '',
};

// Generates pending , fulfilled and rejected action types
export const fetchData = createAsyncThunk('user/fetchUser', async () => {
  return fetch('/api/education').then((res) => res.json());
});

const datalice = createSlice({
  name: 'education',
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchData.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(fetchData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload;
      state.error = '';
    });
    builder.addCase(fetchData.rejected, (state, action) => {
      state.loading = false;
      state.data = [];
      state.error = action.error.message;
    });
  },
});

export default datalice.reducer;
