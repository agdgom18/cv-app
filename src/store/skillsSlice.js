import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const addNewSkill = async (newSkill) => {
  try {
    const response = await fetch('/api/skills', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newSkill),
    });
    console.log(await response.json());
  } catch (error) {
    console.log(error);
  }
};

const initialState = {
  loading: true,
  data: [],
  error: '',
};

// Generates pending , fulfilled and rejected action types
export const fetchSkills = createAsyncThunk('user/fetchSkills', async () => {
  return fetch('/api/skills').then((res) => res.json());
});

const skillSlice = createSlice({
  name: 'skills',
  initialState,
  reducers: {
    updateSkillsList: (state, action) => {
      addNewSkill(action.payload);
      state.data = [action.payload, ...state.data];
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchSkills.pending, (state) => {
      state.loading = true;
    });

    builder.addCase(fetchSkills.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload;
      state.error = '';
    });
    builder.addCase(fetchSkills.rejected, (state, action) => {
      state.loading = false;
      state.data = [];
      state.error = action.error.message;
    });
  },
});
export const { updateSkillsList } = skillSlice.actions;
export default skillSlice.reducer;
