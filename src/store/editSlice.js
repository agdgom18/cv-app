import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  editSkills: false,
};

export const editSlice = createSlice({
  name: 'editStore',
  initialState,
  reducers: {
    changeEditStatusById: (state, action) => {
      switch (action.payload) {
        case 'skills':
          return {
            ...state,
            editSkills: !state.editSkills,
          };
        default:
          return state;
      }
    },
  },
});

export const { changeEditStatusById } = editSlice.actions;

export default editSlice.reducer;
