import { configureStore } from '@reduxjs/toolkit';
import education from './dataSlice';
import skills from './skillsSlice';
import editStore from './editSlice';

const saveStateToLocalStorage = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('reduxState', serializedState);
  } catch (error) {
    console.error('Failed to save state to localStorage', error);
  }
};

const loadStateFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem('reduxState');
    return serializedState ? JSON.parse(serializedState) : undefined;
  } catch (error) {
    console.error('Failed to load state from localStorage', error);
    return undefined;
  }
};

const preloadedState = loadStateFromLocalStorage();

export const store = configureStore({
  reducer: { education, skills, editStore },
  preloadedState,
});

store.subscribe(() => {
  const state = store.getState();
  saveStateToLocalStorage(state);
});
