import './App.scss';
import router from './router/router';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
function App() {
  return (
    <div className="app">
      <RouterProvider router={createBrowserRouter(router)}></RouterProvider>
    </div>
  );
}

export default App;
