import React from 'react';
import { act } from '@testing-library/react';
import App from './App';

describe('Root rendering', () => {
  it('should render App component', () => {
    const rootElement = document.createElement('div');
    rootElement.id = 'root';
    document.body.appendChild(rootElement);

    act(() => {
      require('./index.js');
    });

    const helloWorldText = document.querySelector('div');
    expect(helloWorldText).toBeInTheDocument();
  });
});
