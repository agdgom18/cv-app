# Final Assignment at EPAM Upskill Georgia.Freelancer portfolio site.

### The project was written with:

- ReactJs
- Redux Toolkit
- MirageJs
- react router dom
- react-scroll

[Site ](https://doecv.netlify.app/)

[link to project presentation ](https://drive.google.com/file/d/1j0guTSeK56g18V9RYZ2vZRL3RLCUHZKa/view?usp=sharing)
